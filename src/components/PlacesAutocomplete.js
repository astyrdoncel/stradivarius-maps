import React, { useContext } from "react";
import usePlacesAutocomplete, {
  getGeocode,
  getLatLng,
} from "use-places-autocomplete";
import {
  Combobox,
  ComboboxInput,
  ComboboxPopover,
  ComboboxList,
  ComboboxOption,
} from "@reach/combobox";
import "../css/components/placesAutocomplete.css";
import { MapContext } from "../context/MapContext";

import SearchIcon from "../images/search-icon.svg";
import CloseIcon from "../images/close-icon.svg";

export default function PlacesAutocomplete() {
  const {
    ready,
    value,
    suggestions: { status, data },
    clearSuggestions,
    setValue,
  } = usePlacesAutocomplete({});
  const { places, setPlaces, actualSearch, setActualSearch } =
    useContext(MapContext) || {};

  const handleInput = (e) => {
    let value = e.target.value;
    if (value.length >= 3) setValue(value);
    else setValue(value, false);
  };

  const handleSelect = (val) => {
    setValue(val, false);
    getCoordinates(val);
    clearSuggestions();
  };

  const handleCloseButton = () => {
    setValue("", false);
    clearSuggestions();
  };

  const handleSubmitButton = () => {
    setValue(value, false);
    getCoordinates(value);
  };

  const getCoordinates = (val) => {
    getGeocode({ address: val })
      .then((results) => getLatLng(results[0]))
      .then((latLng) => {
        const { lat, lng } = latLng;
        let newSearch = { description: val, coordinates: { lat, lng } };
        let newPlacesList = [...places];
        newPlacesList.push(newSearch);
        setPlaces(newPlacesList);
        setActualSearch(newSearch);
      })
      .catch((error) => {
        setActualSearch(null);
      });
  };

  const renderSuggestions = () => {
    const suggestions = data.map(({ id, description, place_id }) => (
      <ComboboxOption key={id} value={description} id={place_id} />
    ));

    return suggestions;
  };

  return (
    <Combobox
      className="c-places-autocomplete"
      onSelect={handleSelect}
      aria-labelledby="demo"
    >
      <div className="c-places-autocomplete__list-buttons">
        {value.length > 0 ? (
          <button
            className="c-places-autocomplete__close-button"
            onClick={handleCloseButton}
          >
            <img
              className="c-places-autocomplete__search-image"
              srcSet={CloseIcon}
            ></img>
          </button>
        ) : null}
        <button
          className="c-places-autocomplete__search-button"
          onClick={handleSubmitButton}
        >
          <img
            className="c-places-autocomplete__search-image"
            srcSet={SearchIcon}
          ></img>
        </button>
      </div>
      <ComboboxInput
        id="search-input"
        className="c-places-autocomplete__searchbox"
        value={value}
        onChange={handleInput}
        selectOnClick={true}
        autocomplete={true}
        onBlur={handleInput}
        disabled={!ready}
        placeholder="¿Qué estás buscando?"
      />
      {status === "OK" ? (
        <ComboboxPopover>
          <ComboboxList
            className="c-places-autocomplete__list-box"
            onClick={clearSuggestions}
          >
            {renderSuggestions()}
          </ComboboxList>
        </ComboboxPopover>
      ) : null}
    </Combobox>
  );
}
