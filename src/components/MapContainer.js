import React, { useEffect, useContext, useState } from "react";
import { Map, InfoWindow, Marker, GoogleApiWrapper } from "google-maps-react";
import { MapContext } from "../context/MapContext";

export function MapContainer() {
  const [showingInfoWindow, setShowingInfoWindow] = useState(true);
  const [activeMarker, setActiveMarker] = useState({});
  const [selectedPlace, setSelectedPlace] = useState({});
  const { places, setPlaces, actualSearch } = useContext(MapContext) || {};
  const [centerCoordinates, setCenterCoordinates] = useState({});

  useEffect(() => {
    if (
      actualSearch &&
      actualSearch.coordinates &&
      Object.keys(actualSearch.coordinates).length > 0
    ) {
      setCenterCoordinates(actualSearch.coordinates);
    }
  }, [actualSearch]);

  const onMarkerClick = (props, marker, e) => {
    setShowingInfoWindow(true);
    setActiveMarker(marker);
    setSelectedPlace(props);
  };

  const onMapClicked = (props) => {
    if (showingInfoWindow) {
      setShowingInfoWindow(false);
      setActiveMarker(null);
    }
  };

  const onClickDelete = (selectedPlace) => {
    let filterPlaces = [];
    filterPlaces = places.filter(
      (place) => place.description != selectedPlace.name
    );
    setPlaces(filterPlaces);
  };

  const printMarks = () => {
    if (!places || places.length == 0) return null;
    let marksList = [];
    places.map((location, index) => {
      marksList.push(
        <Marker
          onClick={onMarkerClick}
          key={index}
          title={location.description}
          name={location.description}
          position={location.coordinates}
        />
      );
    });
    return marksList;
  };

  const printMap = () => {
    if (!centerCoordinates) return null;
    return (
      <Map
        initialCenter={{ lat: 41.3887901, lng: 2.1589899 }} //Barcelona
        center={centerCoordinates}
        google={window.google}
        zoom={14}
        onClick={onMapClicked}
      >
        {printMarks()}
        <InfoWindow
          marker={activeMarker}
          onClose={() => onClickDelete(selectedPlace)}
          visible={showingInfoWindow}
        >
          <p className="u-h6">{selectedPlace ? selectedPlace.name : ""}</p>
        </InfoWindow>
      </Map>
    );
  };

  return printMap();
}

export default GoogleApiWrapper({
  apiKey: "AIzaSyCkvgLzrQLwVZXEA3f0-JoCRFJbOiF6OYY",
})(MapContainer);
