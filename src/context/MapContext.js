import React, { useState, createContext } from "react";
export const MapContext = createContext();

const MapProvider = (props) => {

  const [actualSearch, setActualSearch] = useState({});
  const [places, setPlaces] = useState([]);

  return (
    <MapContext.Provider
      value={{
        actualSearch,
        setActualSearch,
        places,
        setPlaces,
      }}
    >
      {props.children}
    </MapContext.Provider>
  );
};
export default MapProvider;
