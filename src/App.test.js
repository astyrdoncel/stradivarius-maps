import React from "react";
import TestRenderer from "react-test-renderer";
import {create, act} from 'react-test-renderer';

import { MapContainer } from "./components/MapContainer";
import PlacesAutocomplete from "./components/PlacesAutocomplete";
import App from "./App";

test("MapContainer render without context", () => {
  expect(TestRenderer.create(<MapContainer />));
});

test("PlacesAutocomplete render without context", () => {
  expect(TestRenderer.create(<PlacesAutocomplete />));
});

test("PlacesAutocomplete search Barcelona", () => {
  let testRenderer;
  act(() => {
    testRenderer = create(<App />);
  });
  const testInstance = testRenderer.root;
  const input = testInstance.findByType("input");
  
  expect(input).toBeDefined();
  input.value = 'Barcelona';
  expect(input.value).toBe('Barcelona')
});
