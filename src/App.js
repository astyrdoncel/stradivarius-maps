import React from "react";
import MapContainer from "./components/MapContainer";
import PlacesAutocomplete from "./components/PlacesAutocomplete";
import MapProvider from "./context/MapContext";

function App() {
  return (
    <MapProvider>
      <PlacesAutocomplete></PlacesAutocomplete>
      <MapContainer></MapContainer>
    </MapProvider>
  );
}

export default App;
