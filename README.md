# README #

Prueba Técnica Frontend para Stradivarius 
Astyr Doncel Chao

### Como preparar el entorno ###

Para clonar este repositorio es necesario que tengas `node` and `npm` instalado globalmente en tu pc/mac.  
Puedes clonarlo desde el siguiente comando: 
`git clone https://bitbucket.org/astyrdoncel/stradivarius-maps.git`.

O bajandote el zip desde la sección `Descargas > Descargar repositorio` del propio Bitcucket.

Instalación:

`npm install`  

Ejecutar Test:  

`npm test`  

Ejecutar aplicación:

`npm start`  

Ver la aplicación:

`localhost:3000` 

### Explicación de la solución ###

El proyecto ha sido realizado siguiendo las tres funcionalidades que se pedían en el enunciado: 

- Posibilidad de ver el mapa con una API Key de Google Maps

- Buscador con sugerencias (Google Places) y poder resaltar partes coincidentes a tiempo real

- Si el usuario selecciona un lugar es guardado en el contexto de la aplicación 

- Si el usuario quiere borrar un Marker debe seleccionar uno y dar al botón de borrar (X) para que desaparezca.

** Librerías externas **

- google-maps-react: Integración de Google Maps (mapa)
- use-places-autocomplete: Integración Google Maps Places (buscador)
- @reach/combobox: Inputs con dropdowns integrados (buscador)

** React Context ** 

Dada mi experiencia con React.Context y el tiempo esperado para realizar esta prueba he considerado utilizar 
esta librería propia de React para manejar el estado de la aplicación, con los mismos resultados que se espera de Redux.

** UI **

Se ha utilizado una interfaz limpia siguiendo el estilo de Material Design con guiños a colores, iconos y tipografía 
de la web de Stradivarius. 

** TEST **

Disponibles en el fichero App.test.js y ejecutables con el comando `npm test`.

** Optimización de la aplicación **

- Bundle de tamaño reducido: Los ficheros y liberarías utilizados están optimizados para ello.

- Poca carga de procesamiento del cliente: Gracias a React Context y los condicionantes, solo se renderizan los componentes cuando el state de la aplicación cambia y, por fuerza mayor, se tiene que volver a renderizar un parte del cliente o su totalidad. 

- Reducir costes de la llamada a la API de Google: Para reducir las llamadas a la API contamos con dos factores para hacer de cortafuegos: el primero, es que solo se hacen llamadas si el texto introducido tiene tres o más caracteres así podemos ahorrar al sistema costes inecesarios cuando la probabilidad de acierto es muy baja; el segundo, gracias a la librería 'use-places-autocomplete' controlamos que solo se hagan llamadas cuando el usuario acabe de escribir una palabra. Permitiendo que los sugeridos solo salten (para llamar a la API) cuando sea realmente necesario para dar buenas opciones al usuario. 

### Contacto ###

- doncel.astyr@gmail.com

- https://www.astyrdoncel.com/